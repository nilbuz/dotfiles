#!/bin/sh

if [ -z "$1" ] || [ -z "$2" ]; then
    echo 'usage: img-to-vid.sh <song> <picture>'
    exit
fi

ffmpeg -hide_banner -loop 1 -i "$2" -i "$1" -shortest -c:v libx264 -preset fast -crf 34 -tune stillimage -pix_fmt yuv420p output.mp4