#!/bin/sh

start_keymap() {
    # wasd to arrow keys
    xmodmap -e "keycode 25 = Up Up"
    xmodmap -e "keycode 38 = Left Left"
    xmodmap -e "keycode 39 = Down Down"
    xmodmap -e "keycode 40 = Right Right"

    # / . , rshift to z x c lshift
    xmodmap -e "keycode 61 = z"
    xmodmap -e "keycode 60 = x"
    xmodmap -e "keycode 59 = c"
    xmodmap -e "keycode 62 = Shift_L"

    # arrow keys to wasd
    xmodmap -e "keycode 111 = w"
    xmodmap -e "keycode 113 = a"
    xmodmap -e "keycode 116 = s"
    xmodmap -e "keycode 114 = d"
}

stop_keymap() {
    # wasd to wasd
    xmodmap -e "keycode 25 = w" 
    xmodmap -e "keycode 38 = a" 
    xmodmap -e "keycode 39 = s" 
    xmodmap -e "keycode 40 = d"

    # / . , rshift to / . , rshift
    xmodmap -e "keycode 61 = slash question"
    xmodmap -e "keycode 60 = period greater"
    xmodmap -e "keycode 59 = comma less"
    xmodmap -e "keycode 62 = Shift_R"

    # arrow keys to arrow keys
    xmodmap -e "keycode 111 = Up"
    xmodmap -e "keycode 113 = Left"
    xmodmap -e "keycode 116 = Down"
    xmodmap -e "keycode 114 = Right"
}


TOGGLE=$HOME/.touhou_keymap_toggle

if [ ! -e "$TOGGLE" ]; then
    touch "$TOGGLE"
    start_keymap
else
    rm "$TOGGLE"
    stop_keymap
fi
