#!/bin/sh

# quit if deadbeef not running
if ! pidof -q deadbeef ; then
    echo 
    exit
fi


# text to display
trackinfo="$(deadbeef --nowplaying '%a — %t ')"
echo "<txt><span font_desc='Impact 9'>$trackinfo</span></txt>"


# tooltip
tooltip=$(deadbeef --nowplaying-tf '%album%  |  %date%  |  %filesize_natural%')
echo "<tool><span font_desc='Impact'>$tooltip</span></tool>"


# click action
txtclick="wmctrl -r DeaDBeeF -b toggle,skip_taskbar,hidden"
echo "<txtclick>$txtclick</txtclick>"


# calculate percent of track played to display in the bar thingy
playback=$(deadbeef --nowplaying-tf '%playback_time_seconds%')
length=$(deadbeef --nowplaying-tf '%length_seconds%')
percent=$(echo "scale=4 ; $playback / $length * 100" | bc)
echo "<bar>$percent</bar>"
