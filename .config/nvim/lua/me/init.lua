require("me.remap")

--local set = vim.opt
vim.opt.number = true
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
vim.opt.softtabstop = 0
vim.opt.expandtab = true
vim.opt.mouse = "a" 
vim.opt.scrolloff = 5
vim.opt.whichwrap = vim.opt.whichwrap + "<,>,[,],h,l"
